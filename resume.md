# Mitchell E. Nielsen

- <inbox@mitchn.me>
- [mitchn.me](https://mitchn.me)
- Minneapolis, MN


## Technical skills

**Certifications**: Red Hat Certified Systems Administrator (RHCSA), Certified Kubernetes Administrator (CKA), Certified Kubernetes Application Developer (CKAD)<br>
**Programming Languages and Tools**: Golang, Operator SDK, Docker, Kubernetes, Helm, Ansible


## Experience

### <span>Prefect, _Senior Platform Enginneer_</span> <span>May 2024 - Present</span>
- Responsible for production cloud infrastructure, service-level objectives, application lifecycle, and monitoring.

### <span>GitLab, _Senior Backend Engineer, Distribution Team_</span> <span>April 2020 - May 2024</span>
 - Promoted to Maintainer of the GitLab Operator for Kubernetes by driving it from proof of concept to general availability, developing the feature set and scheduling issues, becoming the top contributor within the first 6 months
 - Promoted to Maintainer of the GitLab Helm Charts by implementing features, addressing bugs, and triaging issues submitted both internally and by our customers, becoming the top non-maintainer contributor within the first 12 months
 - Supported the GitLab.com delivery team by troubleshooting the SaaS production instance in Kubernetes and developing features in the GitLab Helm Chart to enable better speed and stability

### <span>GE Transportation, _Sr. Build and Release Engineer_</span> <span>August 2018 – April 2020</span>
 - Empowered one of the largest development teams to experience consistent, fault-tolerant application environments by building a deployment pipeline that drives CI/CD for 20+ microservices using Kubernetes, Kustomize, and GitLab CI
 - Enabled cloud-native application teams across the business to increase startup time by developing shared build, test, and release templates that ensure best practices, streamlined deployments, and common toolsets across the business
 - Enhanced quality of code for the company’s products by deploying SonarQube enterprise on Kubernetes, providing 99% uptime by leveraging Kubernetes design best practices and a consistent, automated backup and restore procedure
 - Bolstered product quality and security by integrating Checkmarx, SonarQube, and AquaSec analysis tools into shared pipelines for four major teams by leveraging official CLI tools and embedding them within Docker images for flexible, reusable pipeline stages
 - Inspired team insights and collaboration by providing meaningful information via a portable dashboard solution built on InfluxDB, Grafana, and Python that collects information from Jenkins, Github, Rally, Checkmarx, and Sonarqube

### <span>GE Transportation, _Digital Technology Leadership Program Member_</span> <span>July 2016 – July 2018</span>
 - _Rotation 4_: Cloud Architect - Enabled $991,000 in cost savings by migrating the Manufacturing Execution System to the cloud for a locomotive engine manufacturing plant, responsible for web and app server migration along with build and deploy automation using Chef, Jenkins, AWS EC2/S3/ELB, and Python
 - _Rotation 3_: Field Services Technical Product Manager - Reduced network management costs by $182,000 in 4 months by coordinating the removal of network hardware from customer service centers, providing sites with a 5-10x improvement in connectivity while reducing the company’s technology footprint through an LTE implementation
 - _Rotation 2_: Supply Chain Technical Project Manager – Created build and release automation with Jenkins for a Windows- based Manufacturing Execution System in use at a locomotive engine factory, enabling multiple releases to production per day along with infrastructure monitoring in New Relic
 - _Rotation 1_: DevOps Engineer - Supported the build and release process for a commercial software development team by integrating and automating web security scanning and managing microservices through the company’s IoT platform, Predix, using Jenkins, Bash scripts, and Arachni


## Education

### <span>Master of Science in Information Systems and Operations Management</span> <span>2015 -- 2016</span>
University of Florida: Hough Graduate School of Business | GPA: 3.85 / 4.00

### <span>Bachelor of Science in Business Administration – Information Systems</span> <span>2012 -- 2015</span>
University of Florida: Hough Graduate School of Business | GPA: 3.88 / 4.00
