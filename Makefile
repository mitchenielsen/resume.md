CHROME_PATH ?= "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome"

all:
	python3 resume.py --chrome-path=${CHROME_PATH}
